package fr.istic.project.model.module.out;

import com.jsyn.unitgen.LineOut;

import fr.istic.project.model.memento.Memento;
import fr.istic.project.model.module.AbstractModule;
import fr.istic.project.model.module.filter.FilterAttenuation;
import fr.istic.project.model.observable.PropertyType;
import fr.istic.project.view.module.Component;

public class JSynOut extends AbstractModule implements Out {

    private double attenuationDb;
    private final FilterAttenuation filter;

    public JSynOut() {
        attenuationDb = 0;
        filter = new FilterAttenuation(attenuationDb);
        addEntity(filter);

        LineOut lineOut = new LineOut();
        addInput("input", filter.input);
        addEntity(lineOut);

        filter.getOutput().connect(lineOut.input.getConnectablePart(0));
        filter.getOutput().connect(lineOut.input.getConnectablePart(1));

        addObserver(PropertyType.ATTENUATION_CHANGED, (o, arg) -> filter.setAttenuationDB(getAttenuation()));
    }

    @Override
    public double getAttenuation() {
        return attenuationDb;
    }

    @Override
    public void setAttenuation(double db) {
        if (db > MAX_ATTENUATION) {
            throw new IllegalArgumentException();
        }
        double old = getAttenuation();
        attenuationDb = db;
        firePropertyChange(PropertyType.ATTENUATION_CHANGED, getAttenuation(), old);
    }

    @Override
    public FilterAttenuation getFilter() {
        return filter;
    }

    @Override
    public String toString() {
        return Component.OUT.toString();
    }

    @Override
    public Memento getMemento() {
        Memento m = super.getMemento();
        m.getData().put("attenuation", getAttenuation());

        return m;
    }

    @Override
    public void setMemento(Memento m) {
        if (m == null) {
            throw new IllegalArgumentException();
        }
        super.setMemento(m);
        setAttenuation((Double) m.getData().get("attenuation"));
    }
}
