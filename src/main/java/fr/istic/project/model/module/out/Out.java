package fr.istic.project.model.module.out;

import fr.istic.project.model.module.Module;
import fr.istic.project.model.module.filter.FilterAttenuation;

/**
 * Defines the behavior of a module Out.
 * @inv getAttenuation() &lt;= MAX_ATTENUATION
 * @inv getFilter() != null
 */
public interface Out extends Module {

    double MAX_ATTENUATION = 12;

    /**
     * Returns the filter used for filtering the signal.
     *
     * @return the attenuation filter.
     */
    FilterAttenuation getFilter();

    /**
     * Returns the attenuation.
     *
     * @return the current attenuation.
     */
    double getAttenuation();

    /**
     * Sets the attenuation.
     *
     * @param db the new value.
     * @pre db &lt;= MIN_ATTENUATION
     * @post getAttenuation() == db
     */
    void setAttenuation(double db);
}
